/* eslint-disable react-hooks/exhaustive-deps */
'use client';

import { TechDetails } from '@/lib/types';
import Typography from '@/components/general/typography';
import Link from '@/components/navigation/link';
import ImageWrapper from '@/components/data-display/image-wrapper';
import { useEffect, useState } from 'react';
import { useTheme } from 'next-themes';

const TechDetails = ({ url, logo, darkModeLogo, label,skil }: TechDetails) => {
  const { theme } = useTheme();

  const [bg,setBg]= useState('')
  useEffect(()=>{
    if (skil>80){
      return setBg('#2cfc21')
    } else if (skil<60){
      setBg('#f2ff00')
    } else{
      setBg('red')
    }
  })
  console.log(theme,"ini dari")
  return (
    <div className="flex flex-col items-center gap-2">
      <Link noCustomization href={url} externalLink>
        <ImageWrapper
          src={logo}
          srcForDarkMode={darkModeLogo}
          alt={label}
          className="transition-transform duration-300 md:hover:scale-110"
        />
      </Link>
      <Typography variant="body1">{label}</Typography>
      <div style={{width:'100px',backgroundColor:theme=='dark'?'white':'black',height:'10px',borderRadius:30}}>
      <div style={{height:'10px',backgroundColor:bg,width:`${skil}px`,borderRadius:30}} >
       
      </div>
      
      </div>
      <Typography>{skil}%</Typography>
      
    </div>
  );
};

export default TechDetails;
