'use client';

import Button from '@/components/general/button';
const googleAnalyticsId = process.env.GOOGLE_ANALYTICS_ID;
const DownloadCV = () => {
  return (
    <Button onClick={() =>
      window?.open('https://export-download.canva.com/QIbLw/DAE9vlQIbLw/360/0-2508912829772523667.pdf?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAJHKNGJLC2J7OGJ6Q%2F20231101%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20231101T114407Z&X-Amz-Expires=12429&X-Amz-Signature=7aab602d9847fe5e13104f7ee0c47cbf537c0796e721a2220f37a5916424fb0a&X-Amz-SignedHeaders=host&response-content-disposition=attachment%3B%20filename%2A%3DUTF-8%27%27portfolio.pdf&response-expires=Wed%2C%2001%20Nov%202023%2015%3A11%3A16%20GMT', '_blank')
    }>
      Download CV
    </Button>
  );
};

export default DownloadCV;
