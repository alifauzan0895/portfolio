import Image from "next/image";

import ImageFull from "/public/images/gambarFull.jpg";
import Tag from "@/components/data-display/tag";
import Container from "@/components/layout/container";
import Typography from "@/components/general/typography";
import Link from "@/components/navigation/link";
import { EXTERNAL_LINKS } from "@/lib/data";

const AboutMeSection = () => {
 return (
  <Container className="bg-gray-50" id="about">
   <div className="self-center">
    <Tag label="About me" />
   </div>

   <div className="flex w-full flex-col justify-between gap-12 md:flex-row">
    {/* Image */}
    <div className="flex justify-center md:order-first md:justify-end">
     <div className="relative h-[380px] w-[320px] md:h-[460px] md:w-[380px] lg:h-[520px] lg:w-[440px]">
      <Image
       src={ImageFull}
       alt="Fullpose of Sagar"
       className="absolute z-10 h-[360px] w-[280px] border-8 border-gray-50 max-md:left-5 md:right-0 md:top-0 md:h-[420px] md:w-[340px] lg:h-[480px] lg:w-[400px]"
       style={{ objectFit: "cover" }}></Image>
      <div className="absolute h-[360px] w-[320px] border-8 border-transparent bg-gray-200 max-md:top-5 md:bottom-0 md:left-0 md:h-[420px] md:w-[340px] lg:h-[480px] lg:w-[400px]"></div>
     </div>
    </div>

    {/* Content */}
    <div className="flex max-w-xl flex-col gap-6">
     <Typography variant="h3">
      I &apos;m wholeheartedly passionate about cutting-edge technology,
     </Typography>
     <Typography>
      My unwavering commitment to personal growth drives me to continually
      refine my skills, aspiring to attain a level of excellence. I specialize
      in meticulous systems design, comprehensive systems analysis, and the
      pursuit of proficiency across a diverse array of programming languages.
     </Typography>

     <Typography>
      Over the course of my career, I &apos;ve embraced diverse roles within the
      realm of development. I embarked on my journey as a Front End developer,
      crafting solutions for web and Android applications, before making a
      transition to the Back End, where I honed my expertise in Go-Lang.
     </Typography>
     <Typography>
      Check
{" "}
      <Link
       noCustomization
       externalLink
       withUnderline
       href={EXTERNAL_LINKS.LINKEDIN}>
        LINKEDIN
      </Link>{" "}
      where I share tech-related bites and build in public, or you can follow me
      on{" "}
      <Link
       noCustomization
       externalLink
       withUnderline
       href={EXTERNAL_LINKS.GITHUB}>
       GitHub
      </Link>
      .
     </Typography>
     <Typography>Finally, some quick bits about me.</Typography>
     <div className="flex flex-col gap-2 md:flex-row md:gap-6">
      <ul className="flex list-inside list-disc flex-col gap-2">
       <Typography component="li">F.E Developer</Typography>
       <Typography component="li">Full time freelancer</Typography>
       <Typography component="li">B.E Developer</Typography>
      </ul>
      <ul className="flex list-inside list-disc flex-col gap-2">
       <Typography component="li">Technology Enthusiast</Typography>
       <Typography component="li">Adaptive with anyting Programer Language</Typography>
      </ul>
     </div>
     <Typography>
      One last thing, I&apos;m available for freelance work, so feel free to
      reach out and say hello! I promise I don&apos;t bite 😉
     </Typography>
    </div>
   </div>
  </Container>
 );
};

export default AboutMeSection;
