import { Github, Twitter, Figma } from "lucide-react";

import LogoJavascript from "/public/images/logos/icon-javascript.svg";
import LogoTypescript from "/public/images/logos/icon-typescript.svg";
import LogoReact from "/public/images/logos/icon-react.svg";
import LogoNextjs from "/public/images/logos/icon-nextjs.svg";
import LogoNodejs from "/public/images/logos/icon-nodejs.svg";
import LogoExpress from "/public/images/logos/icon-express.svg";
import LogoExpressLight from "/public/images/logos/icon-express-light.svg";
import LogoPostgreSQL from "/public/images/logos/icon-postgresql.svg";
import LogoTailwindcss from "/public/images/logos/icon-tailwindcss.svg";
import LogoFigma from "/public/images/logos/icon-figma.svg";
import Gologo from "/public/images/logos/gologo.svg";
import Logo2 from "/public/images/logos/icon-git.svg";
import Logo3 from "/public/images/logos/flutter.svg";
import Logo4 from "/public/images/logos/material.svg";

import BankRaya from "/public/images/logos/bankRaya.png";
import Pelindo from "/public/images/logos/pelindo.png";

import ProjectFiskil from "../../public/images/entitree.png"

import AvatarKrisztian from "/public/images/avatar-krisztian.png";
import AvatarEugen from "/public/images/avatar-eugen.png";
import AvatarDummy from "/public/images/avatar-dummy.svg";

import {
 ExperienceDetails,
 ProjectDetails,
 TechDetails,
 TestimonialDetails,
} from "@/lib/types";

export const EXTERNAL_LINKS = {
 GITHUB: "https://github.com/nobitaali",
 GITHUB_REPO: "https://gitlab.com/alifauzan0895/portfolio",
 LINKEDIN: "https://www.linkedin.com/in/ali-fauzan/",
 FIGMA: "https://www.figma.com/@alifauzan08",
 FIGMA_FILE:
 "https://www.figma.com/file/E8KvuTkjY4XmuG7QxnIrgN/Personal-Portfolio-Website?type=design&node-id=328%3A4105&mode=design&t=dLLqYjz3K6c9Fds4-1"
};

export const NAV_LINKS = [
 {
  label: "About",
  href: "#about",
 },
 {
  label: "Work",
  href: "#work",
 },
 {
  label: "Testimonials",
  href: "#testimonials",
 },
 {
  label: "Contact",
  href: "#contact",
 },
];

export const SOCIAL_LINKS = [
 {
  icon: Github,
  url: "https://gitlab.com/alifauzan0895",
 },
 {
  icon: Twitter,
  url: "https://twitter.com/",
 },
 {
  icon: Figma,
  url: "https://www.figma.com/",
 },
];

export const TECHNOLOGIES: TechDetails[] = [
 {
  label: "Javascript",
  skil: 90,
  logo: LogoJavascript,
  url: "https://developer.mozilla.org/en-US/docs/Web/JavaScript",
 },
 {
  label: "Typescript",
  skil: 90,
  logo: LogoTypescript,
  url: "https://www.typescriptlang.org/",
 },
 {
  skil: 90,
  label: "React",
  logo: LogoReact,
  url: "https://react.dev/",
 },
 {
  skil: 90,
  label: "Next.js",
  logo: LogoNextjs,
  url: "https://nextjs.org/",
 },
 {
  skil: 90,
  label: "Node.js",
  logo: LogoNodejs,
  url: "https://nodejs.org/en",
 },
 {
  skil: 90,
  label: "Express.js",
  logo: LogoExpress,
  darkModeLogo: LogoExpressLight,
  url: "https://expressjs.com/",
 },

 {
  skil: 70,
  label: "PostgreSQL",
  logo: LogoPostgreSQL,
  url: "https://www.postgresql.org/",
 },
 {
  skil: 70,
  label: "Flutter",
  logo: Logo3,
  url: "https://docs.flutter.dev/",
 },
 {
  skil: 70,
  label: "GO-LANG",
  logo: Gologo,
  url: "https://go.dev/",
 },

 {
  skil: 70,
  label: "Tailwindcss",
  logo: LogoTailwindcss,
  url: "https://tailwindcss.com/",
 },
 {
  skil: 50,
  label: "Figma",
  logo: LogoFigma,
  url: "https://www.figma.com/",
 },
 {
  skil: 90,
  label: "Git",
  logo: Logo2,
  url: "https://git-scm.com/",
 },
 {
  skil: 90,
  label: "Material Ui",
  logo: Logo4,
  url: "https://git-scm.com/",
 },
];

export const EXPERIENCES: ExperienceDetails[] = [
 {
  logo: BankRaya,
  logoAlt: "Bank Raya Indonesia",
  position: "Back End  ",
  startDate: new Date(2023, 8),
  category:'Fulltime',
  currentlyWorkHere: true,
  summary: [
   "I currently hold the position of Backend Developer at Pinang Dana Talangan where I work with Go Lang and GRPC, utilizing Proto. ",
   "My main responsibilities center around developing crucial APIs for the Paylater service.",
   "Additionally, I m actively involved in debugging and troubleshooting to ensure a seamless operational experience",
   "Furthermore, I play a vital role in supporting the Quality Assurance (QA) team by actively participating in trials and rigorously verifying product quality.",
  ],
 },
 {
  category:'Fulltime',
  logo: BankRaya,
  logoAlt: "Bank Raya Indonesia",
  position: "Front End ",
  endDate: new Date(2023, 8),
  startDate: new Date(2022, 7),
  currentlyWorkHere: false,
  summary: [
   "Develop a Pinang Flexi web interface for BPJS third parties and Dashboard PT. Bank Raya Indonesia",
   "UI Slicing, bug Fixes, SIT, and Integrations",
   "Take Flutter training and the challenge of building a cross-platform mobile application with a Flutter Framework, ",
   "Data Integration, Clean Architecture, and Responsive, Stacked Architecture for State management, Robot Framework Automation Testing",
  ],
 },
 {
  category:'PartTime',
  logo: Pelindo,
  darkModeLogo: Pelindo,
  logoAlt: "PT Pelabuhan Indonesia (Persero) ",
  position: "DevOps & Front End",
  startDate: new Date(2017, 6),
  endDate: new Date(2021, 9),
  summary: [
   "In my curent, role a Freelance Frontend Developer based in PT. Maliya Syahid, currently engaged with ILCS. My primary focus revolves around crafting innovative solutions for monitoring and tracking the performance of PT Pelindo's subsidiary companies. Each of my projects involves conducting in-depth research to select the most fitting framework to create effective entity dashboards.In my role, I take ownership of designing system architecture and implementing clean architecture it using TypeScript with ReactJS. I am deeply committed to delivering responsive, interactive, and intuitive user interfaces, as I strongly believe in providing users with an exceptional experience",
   "Manual Deploy without CI/CD our Pipline To AWS"
  ],
 },
];

export const PROJECTS: ProjectDetails[] = [
 {
  name: "Entitree",
  description:
   "Pelindo Holding and Subholding Management, Dashboard of a Finance Company, and Assignments of Commissioners and Directors.",
  url: "https://fiskil.com.au",
  previewImage: ProjectFiskil,
  technologies: [
   "React",
   "React.js",
   "Typescript",
   "Material UI",
   "Redux Toolkit",
   "React Query",
  ],
 },
];

export const TESTIMONIALS: TestimonialDetails[] = [
 {
  personName: "Krisztian Gyuris",
  personAvatar: AvatarKrisztian,
  title: "Founder - inboxgenie.io",
  testimonial:
   "Job well done! I am really impressed. He is very very good at what he does:) I would recommend Sagar and will rehire in the future for Frontend development.",
 },
 {
  personName: "Eugen Esanu",
  personAvatar: AvatarEugen,
  title: "Founder - shosho.design",
  testimonial:
   "Great guy, highly recommended for any COMPLEX front-end development job! His skills are top-notch and he will be an amazing addition to any team.",
 },
 {
  personName: "Joe Matkin",
  personAvatar: AvatarDummy,
  title: "Freelancer",
  testimonial:
   "Sagar was extremely easy and pleasant to work with and he truly cares about the project being a success. Sagar has a high level of knowledge and was able to work on my MERN stack application without any issues.",
 },
];
